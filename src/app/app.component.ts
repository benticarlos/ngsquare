import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ngSquare';
  lugares:any = [
      {plan:'pagado', cercania: 1, distancia:1, active: true, nombre:'Floristería  La Gardemia'},
      {plan:'gratuito', cercania: 3, distancia:1.8, active: true, nombre:'Dona La Paradita'},
      {plan:'gratuito', cercania: 2, distancia:5, active: true, nombre:'Veterinaria MyPet'},
      {plan:'pagado', cercania: 1, distancia:12, active: false, nombre:'Panaderia Iberia'},
      {plan:'gratuito', cercania: 3, distancia:20, active: true, nombre:'Ferreteria Panamericana'},
      {plan:'gratuito', cercania: 3, distancia:25, active: false, nombre:'Farmacia America'}
  ];
  lat: number = -12.0329209;
  lng: number = -77.0425775;
  //////////////////////////////// Tutorial
  a = 3;
  b = 5;
  listo=true;
  nombre: string = '';
  apellido: string = '';
  constructor() {
    setTimeout(() => {
      this.listo = false;
    }, 3000)
  }

  hacerAlgo() {
    alert('Haciendo algo!');
  }
}
