import {Directive, ElementRef, Input, OnInit, Renderer2} from "@angular/core";

@Directive({
    selector: '[resaltar]'
})
export class ResaltarDirective implements OnInit{
    // el constructor nos permite manipular el DOM
    // el render tambien nos permite manipular el DOM pero mas enfocado al CSS
    //
    constructor(private elRef: ElementRef, private renderer: Renderer2) {}
    // el input
    @Input('resaltar') plan: string = '';
    ngOnInit(){
        if(this.plan === 'pagado') {
            // setStyle recibe 3 parametros (1:elemento nativo,2:atributoCSS,3:ValorAtributo)
            this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'yellow');
            this.renderer.setStyle(this.elRef.nativeElement, 'font-weight', 'bold');
        }
    }
}