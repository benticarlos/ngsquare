import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from "@angular/forms";
import { AgmCoreModule } from "@agm/core";
import {ResaltarDirective} from "./directives/resaltar.directive";
import {ContarClicksDirectives} from "./directives/contar-clicks.directives";

@NgModule({
  declarations: [
    AppComponent,
      ResaltarDirective,
      ContarClicksDirectives
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
      FormsModule,
      AgmCoreModule.forRoot({
        apiKey: 'AIzaSyCiGsoFevMN2J-dXWtD_31AN4UkraR4Hq0'
      })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
