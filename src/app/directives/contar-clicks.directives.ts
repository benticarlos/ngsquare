import {Directive, HostBinding, HostListener} from "@angular/core";
@Directive({
    selector: 'li[contar-clicks]'
})
export class ContarClicksDirectives {
    clickN = 0;
    /// Host Listener y Host Binding
    @HostBinding('style.opacity') opacity: number = .1;
    @HostListener('click', ['$event.target']) onClick(btn) {
        console.log('a', btn, "Número de Clicks: ", this.clickN++);
        this.opacity += .1;
    }
}